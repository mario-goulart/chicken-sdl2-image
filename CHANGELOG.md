
This document describes the changes in each version of chicken-sdl2-image.

This library follows "[semantic versioning](http://semver.org)".
Until version 1.0.0 is released, the API is not guaranteed to be "stable".
That means the maintainer reserves the right to change the API if needed,
possibly in ways that break backwards compatibility with previous versions.
**Large backward-incompatible changes are unlikely**,
but there may be small tweaks and fixes to the API if problems are discovered.

After version 1.0.0 is released, the API is guaranteed to remain stable (no backward-incompatible changes)
until the next new major version (e.g. going from version 1.x to 2.0.0, or 2.x to 3.0.0).


# 0.2.0 (2019-02-13)

- Ported the egg to be compatible with both CHICKEN 4 and CHICKEN 5.

- Improved the way the installer handles compiler and linker flags.
  It will now attempt to automatically discover the correct compiler and linker flags,
  using the `sdl2-config` command.
  In special cases, set the `SDL2_CFLAGS`, `SDL2_LDFLAGS`,
  `SDL2_IMAGE_CFLAGS`, and/or `SDL2_IMAGE_LDFLAGS` environment variables.

- Added support for the SDL2 and SDL2_image frameworks on macOS.
  The installer will automatically use the frameworks
  if `sdl2-config` is not available and the frameworks are installed
  in either the `/Library/Frameworks` or `/System/Library/Frameworks` directories.
  In special cases, set the environment variables described above.


# 0.1.0 (2015-12-19)

Initial release. The following procedures were included:

```
init!               quit!
compiled-version    current-version
load                load*
load-rw             load-rw*
load-typed-rw       load-typed-rw*
```
